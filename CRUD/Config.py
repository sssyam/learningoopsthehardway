from configparser import ConfigParser 

class Parser:
    @classmethod
    def parse(cls,section, key):
        filename = '.config'
        parser = ConfigParser()
        parser.read(filename)
        return parser[section][key]

    @classmethod
    def parse_section(cls,section):
        filename = '.config'
        parser = ConfigParser()
        parser.read(filename)
        return parser[section]

