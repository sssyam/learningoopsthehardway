# README #

This is a personal project to learn OOP and design patterns ( Only used Factory Method ) with python using CRUD operations with Redis, MySQL and MongoDB.
There is a Flask based API Server which exposes CRUD endpoints to be used with Ajax frontend.
The content type requested is json. 

Every call that is being made goes to all the three components.

Tried my best to implement CRUD with Redis, Mongo and MySQL without ORMs and minimal error handling. 
( I have no idea on how to perform checks properly. This is in to-do ).
Want review on the implementation quality and extensibility part. 

Hoping to get guidance on how to improve this.