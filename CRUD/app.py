from flask import Flask, jsonify, request 
from flask_cors import CORS, cross_origin
import DB
import json

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

redis = DB.Redis()
mysql = DB.MySQL()
mongo = DB.Mongo()

@app.route('/', methods=['GET'])
def home():
    return jsonify({"message":"Hello to the CRUD Exercise"})


@app.route('/robots.txt', methods=['GET'])
def help():
    return jsonify({
        "Message": "Help Section",
        "Content":{
            "/" : {
                "Method": "GET",
                "Purpose": "Main Page"
            },
            "/robots.txt":{
                "Method": "GET",
                "Purpose":"This Page"
            },
            "/read":{
                "Method": "GET",
                "Purpose":"Reads from Database"
            },
            "/insert":{
                "Method": "POST",
                "Purpose":"Insert Value Into the Database",
                "Parameters":{
                    "roll":"Roll Number",
                    "name":"Name of Student",
                    "marks": "Marks Scored by Student"
                }
            },
            "/update":{
                "Method": "POST",
                "Purpose":"Update the value Into the Database",
                "Parameters":{
                    "roll":"Roll Number",
                    "name":"Name of Student",
                    "marks": "Marks Scored by Student"
                }
            },
            "/delete":{
                "Method": "POST",
                "Purpose":"Update the value Into the Database",
                "Parameters":{
                    "roll":"Roll Number"
                }
            }
        }
    })


@app.route('/insert', methods=['POST'])
def insert():
    print (request.json)

    x = request.json
    if not ('roll' in x.keys() and 'name' in x.keys() and 'marks' in x.keys() and len(x) == 3 ) :
        return help(), 400

    redis.insert(request.json)
    mysql.insert(request.json)
    mongo.insert(request.json)
    return jsonify({
        "Message":"OK",
        "StatusCode": "200"
    }) , 200

@app.route('/read', methods=['GET'])
def read():
    r = redis.read()
    s = mysql.read()
    m = mongo.read()

    return jsonify({
        "Message":"OK",
        "StatusCode": "200",
        "Result": {
            "Redis": r,
            "MySQL": s,
            "Mongo": m
        }
    }), 200

@app.route('/update', methods=['POST'])
def update():
    print (request.json)

    x = request.json
    if not ('roll' in x.keys() and 'name' in x.keys() and 'marks' in x.keys() and len(x) == 3 ) :
        return help(), 400
    
    redis.update(request.json)
    mysql.update(request.json)
    mongo.update(request.json)
    return jsonify({
        "Message":"OK",
        "StatusCode": "200"
    }), 200

@app.route('/delete', methods=['DELETE'])
def delete():
    print (request.json)
    x = request.json
    if not ('roll' in x.keys() and len(x) == 1 ) :
        return help(), 400
    
    redis.delete(request.json)
    mysql.delete(request.json)
    mongo.delete(request.json)
    return jsonify({
        "Message":"OK",
        "StatusCode": "200"
    }), 200

    

if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True)