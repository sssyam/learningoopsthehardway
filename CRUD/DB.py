from abc import ABC

import redis
import pymysql
import pymongo

from Config import Parser
import Pool  # import RedisPool, MySQLPool, MongoPool

class CRUDBase(ABC):
    def create(self):
        pass
    
    def insert(self, value):
        pass
    
    def read(self):
        pass

    def update(self, value):
        pass

    def delete(self, value):
        pass

class Redis(CRUDBase):
    @classmethod
    def getConnection(cls):
        values = Parser.parse_section('redis')
        obj = redis.Redis(
            host=values['host'], 
            port=int(values['port'])
        )
        return obj

    def __init__(self):
        self.pool = Pool.RedisPool()
        self.hash = Parser.parse("redis","db")
        self.create()

    def create(self):
        connection = self.pool.getConnection()
        pass # Redis Does not need initialization
        self.pool.putConnection(connection)

    def insert(self, value):
        connection = self.pool.getConnection()
        key = value['roll']
        value = '|'.join([
            value['roll'], 
            value['name'], 
            value['marks']
        ])
        connection.hset(self.hash, key, value)
        self.pool.putConnection(connection)

    def read(self):
        connection = self.pool.getConnection()
        data = connection.hgetall(self.hash)
        result = []
        for i in data.keys():
            x = {}
            d = data[i].decode('utf-8').split('|')
            x['roll'] = d[0]
            x['name'] = d[1]
            x['marks'] = d[2]
            result.append(x)
        self.pool.putConnection(connection)
        return result
    
    def update(self, value):
        self.insert(value)

    def delete(self, value):
        connection = self.pool.getConnection()
        connection.hdel(self.hash, value['roll'])
        self.pool.putConnection(connection)


class MySQL(CRUDBase):
    @classmethod
    def getConnection(cls):
        values = Parser.parse_section('mysql')
        obj = pymysql.connect(
            values['host'],  
            values['username'], 
            values['password'], 
            values['db'],
            autocommit=True
        )
        return obj

    def __init__(self):
        self.pool = Pool.MySQLPool()
        self.table = Parser.parse("mysql","table")
        self.create()

    def create(self):
        connection = self.pool.getConnection()
        create_cursor = connection.cursor()
        CREATE_QUERY = f"create table if not exists {self.table} (roll int primary key, name varchar(100), marks int)"
        create_cursor.execute(CREATE_QUERY)
        self.pool.putConnection(connection)

    def insert(self, values):
        connection = self.pool.getConnection()
        insert_cursor = connection.cursor()
        INSERT_QUERY = "insert into " + self.table + " values ( %d, '%s', %d)"
        INSERT_TUPLE = (int(values['roll']), values['name'], int(values['marks']))
        print ( INSERT_QUERY % INSERT_TUPLE )
        insert_cursor.execute( INSERT_QUERY % INSERT_TUPLE )
        self.pool.putConnection(connection)

    def read(self):
        connection = self.pool.getConnection()
        read_cursor = connection.cursor()
        READ_QUERY = "select * from " + self.table
        read_cursor.execute( READ_QUERY )
        result = read_cursor.fetchall()

        data = []
        for d in result:
            x = {}
            x['roll'] = d[0]
            x['name'] = d[1]
            x['marks'] = d[2]
            data.append(x)

        self.pool.putConnection(connection)
        return data

    def update(self, values):
        connection = self.pool.getConnection()
        update_cursor = connection.cursor()
        UPDATE_QUERY = "update " + self.table + " set name = '%s', marks = %d where roll = %d"
        UPDATE_TUPLE = (values['name'], int(values['marks']), int(values['roll']) )
        update_cursor.execute( UPDATE_QUERY % UPDATE_TUPLE )
        self.pool.putConnection(connection)

    def delete(self, values):
        connection = self.pool.getConnection()
        delete_cursor = connection.cursor()
        DELETE_QUERY = "delete from " + self.table + " where roll = %d"
        delete_cursor.execute(DELETE_QUERY % ( int(values['roll']) ))
        self.pool.putConnection(connection)

class Mongo(CRUDBase):
    @classmethod
    def getConnection(cls):
        values = Parser.parse_section('mongo')
        obj = pymongo.MongoClient("mongodb://" + values['host'] + ":" + values['port'])  
        return obj

    def __init__(self):
        self.pool = Pool.MongoPool()
        self.db = Parser.parse("mongo","db")
        self.collection = Parser.parse("mongo","collection")
        self.create()

    def create(self):
        connection = self.pool.getConnection()
        pass # Mongo Does not need initialization
        self.pool.putConnection(connection)

    def insert(self, values):
        connection = self.pool.getConnection()
        connection[self.db][self.collection].insert_one({
            'roll':values['roll'],
            'name':values['name'],
            'marks':values['marks']
        })
        self.pool.putConnection(connection)

    def read(self):
        connection = self.pool.getConnection()
        result = connection[self.db][self.collection].find({},{'_id':0, 'roll':1, 'name':1, 'marks':1})
        self.pool.putConnection(connection)
        data = []
        for d in result:
            x = {}
            x['roll'] = d['roll']
            x['name'] = d['name']
            x['marks'] = d['marks']
            data.append(x)

        return data

    def update(self, values):
        connection = self.pool.getConnection()
        roll = values['roll']
        name = values['name']
        marks = values['marks']
        query_roll = {'roll': roll}
        update_data ={ '$set':{ 'roll':roll, 'name':name , 'marks':marks } }
        connection[self.db][self.collection].update(query_roll, update_data)
        self.pool.putConnection(connection)

    def delete(self, values):
        connection = self.pool.getConnection()
        connection[self.db][self.collection].delete_one({'roll':values['roll']})
        self.pool.putConnection(connection)
