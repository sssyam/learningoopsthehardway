from Config import Parser
from queue import Queue 

# For Abstract Class which cannot be instantiated
from abc import ABC

# Check the type dependencies
import redis
import pymysql
import pymongo

# Database
import DB

class Pool(ABC):

    @classmethod
    def getSize(cls):
        return int(Parser.parse('default','queue_size'))

    #def __init__(self):
    #    self.size = Pool.getSize()

    def getConnection(self):
        pass

    def putConnection(self, object):
        pass

    def getPoolSize():
        pass

class RedisPool(Pool):

    def __init__(self):
        size = Pool.getSize()
        self.queue = Queue(maxsize=size)
        while not self.queue.full():
            self.queue.put(DB.Redis.getConnection())

    def getConnection(self):
        while self.queue.empty():
            self.queue.put(DB.Redis.getConnection())

        return self.queue.get()

    def putConnection(self, connection_object):
        if isinstance(connection_object, redis.client.Redis):
            if not self.queue.full():
                self.queue.put(connection_object)
            else:
                connection_object.close()
        else:
            raise InvalidRedisTypeException()

    def getPoolSize(self):
        return self.queue.size()

    def __del__(self):
        while not self.queue.empty():
            obj = self.queue.get()
            obj.close()


class MySQLPool(Pool):

    def __init__(self):
        size = Pool.getSize()
        self.queue = Queue(maxsize=size)
        while not self.queue.full():
            self.queue.put(DB.MySQL.getConnection())

    def getConnection(self):
        while self.queue.empty():
            self.queue.put(DB.MySQL.getConnection())
        
        return self.queue.get()

    def putConnection(self, connection_object):
        if isinstance(connection_object, pymysql.connections.Connection):
            if not self.queue.full():
                self.queue.put(connection_object)
            else:
                connection_object.close()
        else:
            raise InvalidMySQLTypeException()

    def getPoolSize(self):
        return self.queue.size()

    def __del__(self):
        while not self.queue.empty():
            obj = self.queue.get()
            obj.close()


class MongoPool(Pool):

    def __init__(self):
        size = Pool.getSize()
        self.queue = Queue(maxsize=size)
        while not self.queue.full():
            self.queue.put(DB.Mongo.getConnection())

    def getConnection(self):
        while self.queue.empty():
            self.queue.put = DB.Mongo.getConnection()

        return self.queue.get()

    def putConnection(self, connection_object):
        if isinstance(connection_object, pymongo.mongo_client.MongoClient):
            if not self.queue.full():
                self.queue.put(connection_object)
            else:
                connection_object.close()
        else:
            raise InvalidMongoTypeException()

    def getPoolSize(self):
        return self.queue.size()

    def __del__(self):
        while not self.queue.empty():
            obj = self.queue.get()
            obj.close()


class InvalidRedisTypeException(Exception):
    def __init__(self):
        message = "The Object passed is not a Valid Redis Type Object"
        super().__init__(message)


class InvalidMySQLTypeException(Exception):
    def __init__(self):
        message = "The Object passed is not a Valid MySQL Type Object"
        super().__init__(message)


class InvalidMongoTypeException(Exception):
    def __init__(self):
        message = "The Object passed is not a Valid Mongo Type Object"
        super().__init__(message)
